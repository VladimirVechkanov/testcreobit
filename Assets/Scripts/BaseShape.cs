﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class BaseShape : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    protected int _size = 1;
    [SerializeField]
    protected ShapeType _shapeType;

    public int GetSize => _size;
    public ShapeType GetShapeType => _shapeType;

	public virtual void OnPointerClick(PointerEventData eventData)
	{
		throw new System.NotImplementedException();
	}

    protected void Start()
	{
        transform.localScale = new Vector3(_size, _size, _size);
	}
}

public enum ShapeType : byte
{
    Square = 1,
    Circle = 2,
    Tiangle = 3,
}